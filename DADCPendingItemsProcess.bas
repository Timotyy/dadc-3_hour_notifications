Attribute VB_Name = "DADCPendingItems_Process_Module"
Option Explicit
Public g_strConnection As String
Public cnn As ADODB.Connection

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "Content DElivery at MX1-London"
Public g_strUseremailAddress As String
Public g_strAdministratorEmailAddress As String
Public g_strUseremailName As String
Private Declare Function OpenProcess Lib "kernel32" ( _
    ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" ( _
    ByVal hObject As Long) As Long
Private Declare Function EnumProcesses Lib "PSAPI.DLL" ( _
   lpidProcess As Long, ByVal cb As Long, cbNeeded As Long) As Long
Private Declare Function EnumProcessModules Lib "PSAPI.DLL" ( _
    ByVal hProcess As Long, lphModule As Long, ByVal cb As Long, lpcbNeeded As Long) As Long
Private Declare Function GetModuleBaseName Lib "PSAPI.DLL" Alias "GetModuleBaseNameA" ( _
    ByVal hProcess As Long, ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long
Private Const PROCESS_VM_READ = &H10
Private Const PROCESS_QUERY_INFORMATION = &H400
Public Const TC_UN = 0
Public Const TC_25 = 1
Public Const TC_29 = 2
Public Const TC_30 = 3
Public Const TC_24 = 4

Public Sub DADCPendingItemsProcess()

Dim l_strSQL As String, l_strLocation As String, l_datInterested As Date, l_datInterested2 As Date
Dim l_rsDADCTracker As ADODB.Recordset, l_rsDADCcomments As ADODB.Recordset, l_strBarcode As String, l_strEmailBody As String, l_strEmailBody2 As String, l_strTapeFormat As String
Dim l_lngSec As Long, l_lngHour As Long, SQL As String, SectionTitle As String
Dim l_lngLastProjectManagerContactID As Long, l_strEmailTo As String, l_strEmailToName As String, l_rsEmail As ADODB.Recordset

On Error GoTo ErrorNotification

cnn.Open g_strConnection

Set l_rsDADCTracker = New ADODB.Recordset
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
l_rsDADCTracker.Open SQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    g_strUseremailAddress = l_rsDADCTracker("value")
End If
l_rsDADCTracker.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
l_rsDADCTracker.Open SQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    g_strUseremailName = l_rsDADCTracker("value")
End If
l_rsDADCTracker.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
l_rsDADCTracker.Open SQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    g_strAdministratorEmailAddress = l_rsDADCTracker("value")
End If
l_rsDADCTracker.Close
Set l_rsDADCTracker = Nothing

l_datInterested = DateAdd("h", -4, Now)
Set l_rsDADCTracker = New ADODB.Recordset
Set l_rsDADCcomments = New ADODB.Recordset
Set l_rsEmail = New ADODB.Recordset

'GoTo testingpoint
SectionTitle = "DADC BBC Tracker checks: Things that have come off pending with Tape Location..."
l_strEmailBody = ""

l_strSQL = "SELECT tracker_dadc_itemID, companyID, barcode, itemreference, duedateupload, title, episode, tapelocation FROM tracker_dadc_item WHERE rejected = 0 AND offpendingdate >= '" & FormatSQLDate(l_datInterested) & "' AND readytobill = 0 AND billed = 0;"
Debug.Print l_strSQL
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    While Not l_rsDADCTracker.EOF
        l_strBarcode = Trim(" " & l_rsDADCTracker("barcode"))
        If l_strBarcode <> "" Then
            l_strSQL = "SELECT * FROM tracker_dadc_comment WHERE tracker_dadc_itemID = " & l_rsDADCTracker("tracker_dadc_itemID") & "ORDER BY cdate;"
            l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
            l_strEmailBody = l_strEmailBody & l_strBarcode & " (" & l_rsDADCTracker("itemreference") & ") - " & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
            If Not IsNull(l_rsDADCTracker("duedateupload")) Then
                l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedateupload")
            End If
            l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title"))
            l_strEmailBody = l_strEmailBody & vbCrLf & "Tape Location: " & Trim(" " & l_rsDADCTracker("tapelocation"))
            If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode: " & l_rsDADCTracker("episode")
            If l_rsDADCcomments.RecordCount > 0 Then
                l_rsDADCcomments.MoveLast
                l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
            End If
            l_rsDADCcomments.Close
            l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/dadctracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_dadc_itemID") & vbCrLf & vbCrLf
        End If
        l_rsDADCTracker.MoveNext
    Wend
End If
l_rsDADCTracker.Close
        
Debug.Print l_strEmailBody

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following have come off 'Pending' in the last 4 hours:" & vbCrLf & vbCrLf & l_strEmailBody
    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 19;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "DADC Off-Pending Items", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

SectionTitle = "DADC BBC Tracker Tape checks: Rejected Items"
l_strEmailBody = ""

l_strSQL = "SELECT tracker_dadc_itemID, companyID, barcode, itemreference, duedateupload, title, episode FROM tracker_dadc_item WHERE rejected <> 0 AND pendingdate >= '" & FormatSQLDate(l_datInterested) & "' AND readytobill = 0 AND billed = 0;"
Debug.Print l_strSQL
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    While Not l_rsDADCTracker.EOF
        l_strBarcode = Trim(" " & l_rsDADCTracker("barcode"))
        If l_strBarcode <> "" Then
            l_strSQL = "SELECT * FROM tracker_dadc_comment WHERE tracker_dadc_itemID = " & l_rsDADCTracker("tracker_dadc_itemID") & "ORDER BY cdate;"
            l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
            l_strEmailBody = l_strEmailBody & l_strBarcode & " (" & l_rsDADCTracker("itemreference") & ") - " & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
            If Not IsNull(l_rsDADCTracker("duedateupload")) Then
                l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedateupload")
            End If
            l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title"))
            If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & " Episode: " & l_rsDADCTracker("episode")
            If l_rsDADCcomments.RecordCount > 0 Then
                l_rsDADCcomments.MoveLast
                l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
            End If
            l_rsDADCcomments.Close
            l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/dadctracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_dadc_itemID") & vbCrLf & vbCrLf
        End If
        l_rsDADCTracker.MoveNext
    Wend
End If
l_rsDADCTracker.Close
        
Debug.Print l_strEmailBody

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following have been put on 'Pending' in the last 4 hours:" & vbCrLf & vbCrLf & l_strEmailBody
    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 17;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "DADC Pending Items", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

SectionTitle = "DADC BBC Tracker Tape checks: Decision Tree Items"
l_strEmailBody = ""

l_strSQL = "SELECT tracker_dadc_itemID, companyID, barcode, itemreference, duedateupload, title, episode FROM tracker_dadc_item WHERE decisiontree <> 0 AND decisiontreedate >= '" & FormatSQLDate(l_datInterested) & "' AND readytobill = 0 AND billed = 0;"
Debug.Print l_strSQL
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    While Not l_rsDADCTracker.EOF
        l_strBarcode = Trim(" " & l_rsDADCTracker("barcode"))
        If l_strBarcode <> "" Then
            l_strSQL = "SELECT * FROM tracker_dadc_comment WHERE tracker_dadc_itemID = " & l_rsDADCTracker("tracker_dadc_itemID") & "ORDER BY cdate;"
            l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
            l_strEmailBody = l_strEmailBody & l_strBarcode & " (" & l_rsDADCTracker("itemreference") & ") - " & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
            If Not IsNull(l_rsDADCTracker("duedateupload")) Then
                l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedateupload")
            End If
            l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title"))
            If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & " Episode: " & l_rsDADCTracker("episode")
            If l_rsDADCcomments.RecordCount > 0 Then
                l_rsDADCcomments.MoveLast
                l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment") & vbCrLf
                If Val(Trim(" " & l_rsDADCcomments("emailclock"))) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will replace Clock." & vbCrLf
                If Val(Trim(" " & l_rsDADCcomments("emailbarsandtone"))) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will replace Bars and Tone." & vbCrLf
                If Val(Trim(" " & l_rsDADCcomments("emailtimecode"))) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will re-stripe the Timecode." & vbCrLf
                If Val(Trim(" " & l_rsDADCcomments("emailblackatend"))) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will add correct Black at End." & vbCrLf
            End If
            l_rsDADCcomments.Close
            l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/dadctracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_dadc_itemID") & vbCrLf & vbCrLf
        End If
        l_rsDADCTracker.MoveNext
    Wend
End If
l_rsDADCTracker.Close
        
Debug.Print l_strEmailBody

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following have been discovered to have Decision Tree issues in the last 4 hours:" & vbCrLf & vbCrLf & l_strEmailBody
    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 21;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "DADC Decision Tree Items", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

SectionTitle = "DADC BBC Tracker: KPI Timing warnings - 48 hrs"

l_strSQL = "SELECT tracker_dadc_itemID, companyID, barcode, itemreference, duedateupload, title, episode, decisiontree, decisiontreedate, rejected, pendingdate, offpendingdate, stagefield2 FROM tracker_dadc_item WHERE stagefield4 IS NULL AND stagefield2 IS NOT NULL AND stagefield2 <= '" & FormatSQLDate(l_datInterested) & "' AND readytobill = 0 AND billed = 0 AND companyID = 1261;"
Debug.Print l_strSQL
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    While Not l_rsDADCTracker.EOF
        l_lngSec = 0
        'Check pending
        If Not IsNull(l_rsDADCTracker("pendingdate")) Then
            If IsNull(l_rsDADCTracker("offpendingdate")) Then
                GoTo SKIP_ITEM
            Else
                If l_rsDADCTracker("stagefield2") < l_rsDADCTracker("pendingdate") Then
                    l_lngSec = DateDiff("s", l_rsDADCTracker("stagefield2"), l_rsDADCTracker("pendingdate"))
                End If
                l_lngSec = l_lngSec + DateDiff("s", l_rsDADCTracker("offpendingdate"), Now)
                If l_lngSec / 3600 < 36 Then
                    GoTo SKIP_ITEM
                End If
            End If
        End If
        'Check Decisiontree
        If Val(Trim(" " & l_rsDADCTracker("decisiontree"))) <> 0 Then
            If l_rsDADCTracker("stagefield2") >= DateAdd("h", -24, l_datInterested) Then GoTo SKIP_ITEM
        ElseIf Val(Trim(" " & l_rsDADCTracker("rejected"))) <> 0 Then
            GoTo SKIP_ITEM
        End If
        'Note it
        l_strBarcode = Trim(" " & l_rsDADCTracker("barcode"))
        If l_strBarcode <> "" Then
            l_strSQL = "SELECT * FROM tracker_dadc_comment WHERE tracker_dadc_itemID = " & l_rsDADCTracker("tracker_dadc_itemID") & "ORDER BY cdate;"
            l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
            l_strEmailBody = l_strEmailBody & l_strBarcode & " (" & l_rsDADCTracker("itemreference") & ") - " & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
            If Not IsNull(l_rsDADCTracker("duedateupload")) Then
                l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedateupload")
            End If
            l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title"))
            If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & " Episode: " & l_rsDADCTracker("episode")
            If l_rsDADCcomments.RecordCount > 0 Then
                l_rsDADCcomments.MoveLast
                l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
            End If
            l_rsDADCcomments.Close
            l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.jca.tv/dadctracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_dadc_itemID") & vbCrLf & vbCrLf
        End If
'Carry on
SKIP_ITEM:
        l_rsDADCTracker.MoveNext
    Wend
End If
l_rsDADCTracker.Close
        
Debug.Print l_strEmailBody

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following have failed the KPI for completion:" & vbCrLf & vbCrLf & l_strEmailBody

End If

l_datInterested2 = DateAdd("h", -36, Now)

SectionTitle = "DADC BBC Tracker: KPI Timing warnings - 48+12 hrs"

l_strSQL = "SELECT tracker_dadc_itemID, companyID, barcode, itemreference, duedateupload, title, episode, decisiontree, decisiontreedate, rejected, pendingdate, offpendingdate, stagefield2 FROM tracker_dadc_item WHERE stagefield4 IS NULL AND stagefield2 IS NOT NULL AND stagefield2 >= '" & FormatSQLDate(l_datInterested) & "' AND stagefield2 <= '" & FormatSQLDate(l_datInterested2) & "' AND readytobill = 0 AND billed = 0 AND companyID = 1261;"
Debug.Print l_strSQL
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    While Not l_rsDADCTracker.EOF
        l_lngSec = 0
        'Check pending
        If Not IsNull(l_rsDADCTracker("pendingdate")) Then
            If IsNull(l_rsDADCTracker("offpendingdate")) Then
                GoTo SKIP_ITEM2
            Else
                If l_rsDADCTracker("stagefield2") < l_rsDADCTracker("pendingdate") Then
                    l_lngSec = DateDiff("s", l_rsDADCTracker("stagefield2"), l_rsDADCTracker("pendingdate"))
                End If
                l_lngSec = l_lngSec + DateDiff("s", l_rsDADCTracker("offpendingdate"), Now)
                If l_lngSec / 3600 < 36 Then
                    GoTo SKIP_ITEM2
                End If
            End If
        End If
        'Check Decisiontree
        If Val(Trim(" " & l_rsDADCTracker("decisiontree"))) <> 0 Then
            If l_rsDADCTracker("stagefield2") >= DateAdd("h", -24, l_datInterested) Then GoTo SKIP_ITEM2
        ElseIf Val(Trim(" " & l_rsDADCTracker("rejected"))) <> 0 Then
            GoTo SKIP_ITEM2
        End If
        'Note it
        l_strBarcode = Trim(" " & l_rsDADCTracker("barcode"))
        If l_strBarcode <> "" Then
            l_strSQL = "SELECT * FROM tracker_dadc_comment WHERE tracker_dadc_itemID = " & l_rsDADCTracker("tracker_dadc_itemID") & "ORDER BY cdate;"
            l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
            l_strEmailBody2 = l_strEmailBody2 & l_strBarcode & " (" & l_rsDADCTracker("itemreference") & ") - " & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
            If Not IsNull(l_rsDADCTracker("duedateupload")) Then
                l_strEmailBody2 = l_strEmailBody2 & " Due Date: " & l_rsDADCTracker("duedateupload")
            End If
            l_strEmailBody2 = l_strEmailBody2 & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title"))
            If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody2 = l_strEmailBody2 & " Episode: " & l_rsDADCTracker("episode")
            If l_rsDADCcomments.RecordCount > 0 Then
                l_rsDADCcomments.MoveLast
                l_strEmailBody2 = l_strEmailBody2 & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
            End If
            l_rsDADCcomments.Close
            l_strEmailBody2 = l_strEmailBody2 & vbCrLf & "Link to Web Tracker: http://online.jca.tv/dadctracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_dadc_itemID") & vbCrLf & vbCrLf
        End If
'Carry on
SKIP_ITEM2:
        l_rsDADCTracker.MoveNext
    Wend
End If
l_rsDADCTracker.Close
        
Debug.Print l_strEmailBody2

If l_strEmailBody2 <> "" Or l_strEmailBody <> "" Then

    l_strEmailBody2 = "The following have are within 12 hours of failing the KPI for completion:" & vbCrLf & vbCrLf & l_strEmailBody2
    l_strEmailBody = l_strEmailBody & l_strEmailBody2 & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 23;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "DADC KPI Items", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

Set l_rsDADCcomments = Nothing
Set l_rsDADCTracker = Nothing
Set l_rsEmail = Nothing

cnn.Close

Exit Sub

ErrorNotification:

SendSMTPMail g_strAdministratorEmailAddress, "", "DADC 3 Hour Notifications Generated an error", "Error number: " & Err.Number & vbCrLf & "Error Description: " & Err.Description & vbCrLf & "Section: " & SectionTitle, "", True, "", "", ""
End

End Sub

Private Function IsProcessRunning(ByVal sProcess As String) As Boolean

Const MAX_PATH As Long = 260

Dim lProcesses() As Long, lModules() As Long, N As Long, lRet As Long, hProcess As Long
Dim sName As String
        
sProcess = UCase$(sProcess)
ReDim lProcesses(1023) As Long
If EnumProcesses(lProcesses(0), 1024 * 4, lRet) Then
    For N = 0 To (lRet \ 4) - 1
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, lProcesses(N))
        If hProcess Then
            ReDim lModules(1023)
            If EnumProcessModules(hProcess, lModules(0), 1024 * 4, lRet) Then
                sName = String$(MAX_PATH, vbNullChar)
                GetModuleBaseName hProcess, lModules(0), sName, MAX_PATH
                sName = Left$(sName, InStr(sName, vbNullChar) - 1)
                If Len(sName) = Len(sProcess) Then
                    If sProcess = UCase$(sName) Then IsProcessRunning = True: Exit Function
                End If
            End If
        End If
        CloseHandle hProcess
    Next N
End If
End Function
Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & lp_strEmailToAddress & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & lp_strCopyToEmailAddress & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & lp_strBCCAddress & "', ", "Null, ")
SQL = SQL & "'" & lp_strSubject & "', "
SQL = SQL & "'" & lp_strBody & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & lp_strAttachment & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & lp_strEmailFromAddress & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function



