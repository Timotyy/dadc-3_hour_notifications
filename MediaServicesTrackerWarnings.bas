Attribute VB_Name = "Process_Module"
Option Explicit
Public g_strConnection As String
Public cnn As ADODB.Connection

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "Content Delivery at MX1-London"
Public g_strUseremailAddress As String
Public g_strAdministratorEmailAddress As String
Public g_strUseremailName As String
Private Declare Function OpenProcess Lib "kernel32" ( _
    ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" ( _
    ByVal hObject As Long) As Long
Private Declare Function EnumProcesses Lib "PSAPI.DLL" ( _
   lpidProcess As Long, ByVal cb As Long, cbNeeded As Long) As Long
Private Declare Function EnumProcessModules Lib "PSAPI.DLL" ( _
    ByVal hProcess As Long, lphModule As Long, ByVal cb As Long, lpcbNeeded As Long) As Long
Private Declare Function GetModuleBaseName Lib "PSAPI.DLL" Alias "GetModuleBaseNameA" ( _
    ByVal hProcess As Long, ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long
Private Const PROCESS_VM_READ = &H10
Private Const PROCESS_QUERY_INFORMATION = &H400
Public Const TC_UN = 0
Public Const TC_25 = 1
Public Const TC_29 = 2
Public Const TC_30 = 3
Public Const TC_24 = 4

Public Sub MediaServicesTrackerWarningsProcess()

Dim l_strSQL As String, l_strLocation As String, l_datInterested As Date, l_datInterested2 As Date
Dim l_rsTracker As ADODB.Recordset, l_rsComments As ADODB.Recordset, l_strBarcode As String, l_strEmailBody As String, l_strEmailBody2 As String, l_strTapeFormat As String
Dim l_lngSec As Long, l_lngHour As Long, SQL As String, SectionTitle As String
Dim l_lngLastProjectManagerContactID As Long, l_strEmailTo As String, l_strEmailToName As String, l_rsEmail As ADODB.Recordset

On Error GoTo ErrorNotification

cnn.Open g_strConnection

Set l_rsTracker = New ADODB.Recordset
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
l_rsTracker.Open SQL, cnn, 3, 3
If l_rsTracker.RecordCount > 0 Then
    g_strUseremailAddress = l_rsTracker("value")
End If
l_rsTracker.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
l_rsTracker.Open SQL, cnn, 3, 3
If l_rsTracker.RecordCount > 0 Then
    g_strUseremailName = l_rsTracker("value")
End If
l_rsTracker.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
l_rsTracker.Open SQL, cnn, 3, 3
If l_rsTracker.RecordCount > 0 Then
    g_strAdministratorEmailAddress = l_rsTracker("value")
End If
l_rsTracker.Close
Set l_rsTracker = Nothing

Set l_rsTracker = New ADODB.Recordset
Set l_rsComments = New ADODB.Recordset
Set l_rsEmail = New ADODB.Recordset

'GoTo testingpoint
SectionTitle = "Media Services Tracker Due Date Warnings ... "
l_strEmailBody = ""

l_strSQL = "SELECT * FROM vw_MediaServices_UrgentTasks;"
Debug.Print l_strSQL
l_rsTracker.Open l_strSQL, cnn, 3, 3
If l_rsTracker.RecordCount > 0 Then
    l_rsTracker.MoveFirst
    Do While Not l_rsTracker.EOF
        l_strSQL = "SELECT * FROM tracker_ms_comment WHERE tracker_ms_itemID = " & l_rsTracker("tracker_ms_itemID") & "ORDER BY cdate;"
        l_rsComments.Open l_strSQL, cnn, 3, 3
        l_strEmailBody = l_strEmailBody & "Item: " & l_rsTracker("tracker_ms_itemID") & ", "
        l_strEmailBody = l_strEmailBody & "Type: " & l_rsTracker("MS_Request_type") & ", "
        l_strEmailBody = l_strEmailBody & "Priority: " & l_rsTracker("Priority") & vbCrLf
        l_strEmailBody = l_strEmailBody & "Date: " & l_rsTracker("Request_Date") & ", "
        l_strEmailBody = l_strEmailBody & "DueBy: " & l_rsTracker("DueBy") & vbCrLf
        l_strEmailBody = l_strEmailBody & "By: " & l_rsTracker("Request_by") & "("
        l_strEmailBody = l_strEmailBody & l_rsTracker("REquester_Email") & ")" & ", "
        l_strEmailBody = l_strEmailBody & "Assigned To: " & IIf(IsNull(l_rsTracker("AssignedTo")) Or l_rsTracker("AssignedTo") = "", "Nobody", l_rsTracker("AssignedTo")) & vbCrLf
        l_strEmailBody = l_strEmailBody & "Title: " & l_rsTracker("Title") & vbCrLf
        l_strEmailBody = l_strEmailBody & "Request: " & l_rsTracker("Request") & vbCrLf
        If l_rsComments.RecordCount > 0 Then
            l_rsComments.MoveLast
            l_strEmailBody = l_strEmailBody & "Last Comment (" & l_rsComments("cdate") & ") By: " & l_rsComments("cuser") & "Comment: " & l_rsComments("comment") & vbCrLf
        End If
        l_rsComments.Close
        l_strEmailBody = l_strEmailBody & vbCrLf
        l_rsTracker.MoveNext
    Loop
End If
l_rsTracker.Close
        
Debug.Print l_strEmailBody

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following Media Services Tracker items need to be completed in the next Hour::" & vbCrLf & vbCrLf & l_strEmailBody & vbCrLf
    SendSMTPMail "UK-MediaServices@mx1.com", "Media Services Team", "Media Servics Tracker Items due in next hour", l_strEmailBody, "", True, "", "", ""

End If

Set l_rsComments = Nothing
Set l_rsTracker = Nothing

cnn.Close

Exit Sub

ErrorNotification:

SendSMTPMail g_strAdministratorEmailAddress, "", "DADC 3 Hour Notifications Generated an error", "Error number: " & Err.Number & vbCrLf & "Error Description: " & Err.Description & vbCrLf & "Section: " & SectionTitle, "", True, "", "", ""
End

End Sub

Private Function IsProcessRunning(ByVal sProcess As String) As Boolean

Const MAX_PATH As Long = 260

Dim lProcesses() As Long, lModules() As Long, N As Long, lRet As Long, hProcess As Long
Dim sName As String
        
sProcess = UCase$(sProcess)
ReDim lProcesses(1023) As Long
If EnumProcesses(lProcesses(0), 1024 * 4, lRet) Then
    For N = 0 To (lRet \ 4) - 1
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, lProcesses(N))
        If hProcess Then
            ReDim lModules(1023)
            If EnumProcessModules(hProcess, lModules(0), 1024 * 4, lRet) Then
                sName = String$(MAX_PATH, vbNullChar)
                GetModuleBaseName hProcess, lModules(0), sName, MAX_PATH
                sName = Left$(sName, InStr(sName, vbNullChar) - 1)
                If Len(sName) = Len(sProcess) Then
                    If sProcess = UCase$(sName) Then IsProcessRunning = True: Exit Function
                End If
            End If
        End If
        CloseHandle hProcess
    Next N
End If
End Function
Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function



